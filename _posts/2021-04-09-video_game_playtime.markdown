---
layout: post
title:  "How I Learned to Stop Thinking of Games in Terms of Price-per-Playtime"
date:   2021-04-09 00:00:00 +0800
categories: posts
published: true
author: Paco Halili
---

I remember the first purchase I ever made with money earned from work. It was 2013, and I had just gotten my first ever salary from my college internship. It wasn't a huge sum, but it was special to me because hey, I (quite literally) earned it! After years of conditioning myself to save and always choose the cheaper option, I finally had the means to splurge on something for myself. It was just the first paycheck of many, after all[^1][^2]. So with paycheck in hand, I went to the mall and for the first time in a long time bought something I had no real *need* for: a copy of **The Elder Scrolls V: Skyrim - Legendary Edition**.

![The Elder Scrolls V: Skyrim]({{site.url}}/assets/images/20210409-skyrim.jpg){: .center-image }

My thinking for this was simple: I knew of Skyrim to be a massive open-world game with lots of things to do, so I'll definitely get my money's worth (and then some) for my PHP 1800.00 purchase[^3], especially when you take the DLC into account. I've played open world games before, and I spent a lot of time on those as well. The freedom to do whatever I wanted to appealed to me, and I was sure I was going to get a lot of mileage out of it. And to be fair, I was right! I finished all the main story and DLC quests on my first playthrough after a hundred hours, then I immediately made a new character with a more optimized build[^4]. I spent around 150 hours with *that* character, bringing my total to roughly 250 hours of gameplay. Over the course of two months[^5], I essentially paid for entertainment at a rate of PHP 7.00/hour. Not a bad deal, right?

## Well, it wasn't a *great* deal...

Now, I didn't have this realization right away. But out of those 250 hours, I'd say maybe 50 of those were "peak" fun for me. I'm talking about the times when I was completely immersed in the experience as opposed to me sitting there thinking "Okay, right now I have to grind my Archery skill by repeatedly shooting arrows at an immortal monk[^6] so I could have an easier time getting to the next fun part of the game." 

I did have a lot to do in that game, but with that freedom came a lot of filler. There are only a handful of major quests, and you spend most of the time either trying to get from one point in the story to the next, grinding your skills just enough to get that next nice gear, or doing some inconsequential side quest that was randomly generated. Don't get me wrong, I love the game. I understand what sort of game it is, and the freedom it affords the player is a big reason why I and many others like it so much. But back then, my main factor in even considering buying that game was purely the price-per-playtime I knew I would get. It wasn't until later when I would change this line of thinking.

## Entering uncharted territory

I had the same thought process for all the next games I would play in the following years. My next couple of video game purchases were either JRPGs I know would last me a long time, or compilations of multiple games for the price of one. The thinking behind it was the same, longer games meant more bang for my buck. I was already working full time by this point, so I did not have as much free time to devote to gaming. Then one day, I received a bunch of old PS3 games basically for free. One of them was the 2009 game **Uncharted 2: Among Thieves**. 

![Uncharted 2: Among Thieves]({{site.url}}/assets/images/20210409-uncharted.jpg){: .center-image }

I tried it out one Friday evening after work and once it got going... I couldn't put it down. For the benefit of those who haven't played the game, Uncharted 2 is widely regarded as a masterclass in how to design a well-paced action/adventure game. The main gameplay beats are (roughly speaking) exploration, shooting, and puzzles. Just as you start to get sick of one part of the game, it goes on to the next, and so on and so on. It was enough to get me hooked. I spent that whole weekend playing the game start to finish, barely leaving the house[^7]. 

Once I finished the approximately 12 hour adventure, I realized that while I spent significantly less time with Uncharted 2 compared to an open world RPG like Skyrim, 100% of that 12 hour playtime was spent *having as much fun as I could possibly have*. There was no "downtime" in the game where I had to do something unfun just to get to the part that was fun. I didn't spend money on the game, but I feel like even if I had, I would still see it as a worthwhile purchase. That became the turning point for me. 

Because of that experience, I became more comfortable buying and playing shorter story-driven games. I picked up games that were great sub-15 hour experiences[^8] because I ditched the idea that I had to get as much playtime from a game for it to be worth the money. I still have space in my library for those big open world video games because frankly, I still enjoy them! But if you ask me to list down all the best games I've played in the last decade, it would be those short, linear games that gave me the most meaningful experiences.

[^1]: Assuming I wouldn't lose the job for whatever reason...
[^2]: (I didn't)
[^3]: Roughly 40USD
[^4]: I put points into *Speech* for my first character.
[^5]: 250 hours of video games in a span of two months. I miss having all that free time.
[^6]: In case you're not aware, this is a completely valid way of raising the archery skill.
[^7]: Although to be fair, I probably wouldn't have left the house anyway.
[^8]: Incidentally, those are the type of games I replay most often. So even going by my previous price-per-playtime paradigm, I still get the most value from those games :^)
